export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCdie0jBWf4PchIVsklkd1SlEzdaPitYOA',
    authDomain: 'angularjs-redux.firebaseapp.com',
    databaseURL: 'https://angularjs-redux.firebaseio.com',
    projectId: 'angularjs-redux',
    storageBucket: 'angularjs-redux.appspot.com'
  }
};
