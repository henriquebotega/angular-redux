import { filter } from 'rxjs/operators';
export interface ProductsState {
  data: any;
  currentProduct: any;
  error: string | undefined;
  loading: boolean;
}

export const initialState: ProductsState = {
  data: [],
  currentProduct: undefined,
  loading: false,
  error: undefined
};

export function reducer(
  state = initialState,
  action
): ProductsState {

  switch (action.type) {

    case '[Products] Add Product':

    const products = [...state.data, action.payload];

    return {
      ...state,
      data: products
    };

    case '[Products] Remove Product':

    const newProducts = state.data.filter( product => {
      return product.id !== action.payload;
    });

    return {
      ...state,
      data: newProducts
    };

    case '[Products] Load Product by Id':

    const currentProduct = state.data.find( product => {
      return product.id === parseInt(action.payload, 10 );
    });


    return {
      ...state,
      currentProduct: currentProduct
    };

    case '[Products] Edit Product':

    const editedProduct = {id: parseInt(action.payload.id, 10), ...action.payload.data};
    const updatedProducts = state.data.map( product => {
      return product.id !== parseInt(action.payload.id, 10 ) ?
      product : editedProduct;
    });

    return {
      ...state,
      currentProduct: editedProduct,
      data: updatedProducts
    };

    // case '[Products] Load Products Success':

    // return {
    //   ...state,
    //   data: action.payload
    // };

    // case '[Products] Load Product by Id Success':

    // return {
    //   ...state,
    //   currentProduct: action.payload
    // };

    default:
    return state;
  }
}
