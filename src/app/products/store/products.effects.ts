import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import {
  tap,
  map,
  catchError,
  switchMap,
  concatMap,
  filter,
  mergeMap
} from 'rxjs/operators';
import { of } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import {
  LoadProductsSuccess,
  AddProductSuccess,
  RemoveProductSuccess,
  LoadProductByIdSuccess,
  EditProductSuccess
} from './products.actions';


@Injectable()
export class ProductEffects {
  constructor(
    private actions: Actions,
    private db: AngularFirestore
  ) {
  }

  @Effect()
  loadProducts$ = this.actions.pipe(
    ofType(
      '[Products] Load Products'
    ),
    mergeMap(() => {
      return this.db.collection('products');
      /*
      return this.db.collection('products').snapshotChanges().pipe(
        map((actions) => {
          const products = actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            console.log(data)
            return { id, ...data };
          });

          return new LoadProductsSuccess(products);
        })
     );
      */
    })
  );

  @Effect()
  loadById$ = this.actions.pipe(
    ofType(
      '[Products] Load Product by Id'
    ),
    mergeMap((action: any) => {
      return this.db.collection('products').doc(action.payload).get().pipe(
        filter(snapshot => snapshot.exists), map((snapshot) => {
          return new LoadProductByIdSuccess({ id: action.payload, ...snapshot.data() });
        })
      )
    })
  );

  @Effect()
  addProduct$ = this.actions.pipe(
    ofType(
      '[Products] Add Product'
    ),
    mergeMap((action: any) => {
      return of(this.db.collection('products').add(action.payload)).pipe(map(() => new AddProductSuccess()));
    })
  );

  @Effect()
  removeProduct$ = this.actions.pipe(
    ofType(
      '[Products] Remove Product'
    ),
    mergeMap((action: any) => {
      return of(this.db.collection('products').doc(action.payload).delete()).pipe(map(() => new RemoveProductSuccess()));
    })
  );

  @Effect()
  editProduct$ = this.actions.pipe(
    ofType(
      '[Products] Edit Product'
    ),
    mergeMap((action: any) => {
      return of(this.db.collection('products').doc(action.payload.id).set(action.payload.data)).pipe(map(() => new EditProductSuccess()));
    })
  );
}
